const express = require("express");
const router = express.Router();
const {exchangeRates} = require("../src/utils");

router.get("/", (req,res)=>{
    return res.send({rates:exchangeRates});
})

// Router.get("/rates",(req,res)=>{

// })

module.exports = router;