const {expect} = require("chai");

const {exchange,exchangeRates} = require("../src/utils");

it("test_exchange_usd_to_php", ()=>{
    const phpValue = exchange("usd","peso",1000);
    expect(phpValue).to.equal(50730);
})

it("test_exchange_usd_to_yen", ()=>{
    const phpValue = exchange("usd","yen",1000)
    expect(phpValue).to.equal(108630);
})

it("test_exchange_usd_to_yuan", ()=>{
    const phpValue = exchange("usd","yuan",1000)
    expect(phpValue).to.equal(7030);
})

it("test_exchange_usd_to_won", ()=>{
    const phpValue = exchange("usd","won",1000)
    expect(phpValue).to.equal(1187240);
})

it("test_exchange_yen_to_peso",()=>{
    const phpValue = exchange("yen","peso",100)
    expect(phpValue).to.equal(74)
})

it("test_exchange_yen_to_usd", ()=>{
    const phpValue = exchange("yen","usd",10000)
    expect(phpValue).to.equal(92)
})

it("test_exchange_yen_to_won", ()=>{
    const phpValue = exchange("yen","won",100)
    expect(phpValue).to.equal(1093)
})

it("test_exchange_yen_to_yuan", ()=>{
    const phpValue = exchange("yen","yuan",100)
    expect(phpValue).to.equal(6.5)
})

describe("test_exchange_peso_to_multiple_currencies",()=>{
    it("test_exchange_peso_to_usd", ()=>{
        const phpValue = exchange("peso","usd",100)
        expect(phpValue).to.equal(2)
    })
    
    it("test_exchange_peso_to_won", ()=>{
        const phpValue = exchange("peso","won",100)
        expect(phpValue).to.equal(2339)
    })
    
    it("test_exchange_peso_to_yen", ()=>{
        const phpValue = exchange("peso","yen",100)
        expect(phpValue).to.equal(214)
    })
    
    it("test_exchange_peso_to_yuan", ()=>{
        const phpValue = exchange("peso","yuan",1000)
        expect(phpValue).to.equal(140)
    })
})
