const chai = require("chai");
const {expect} = chai;
const chaiHttp = require("chai-http");
chai.use(chaiHttp);

const serverUrl = "http://localhost:3000";

describe("GET ALL RATES",()=>{
    it("should accept http", ()=>{
        chai.request(serverUrl).get("/rates")
        .end((err,res)=>expect(res).to.not.equal(undefined))
    })

    it("should have a status of 200", (done)=>{
        chai.request(serverUrl).get("/rates")
        .end((err,res)=>expect(res).to.have.status(200))
        done();
    })

    it("should return an object w/ a property of rates", ()=>{
        chai.request(serverUrl).get("/rates")
        .end((err,res)=>{
            console.log(res.body)
            expect(res.body).to.have.property("rates")
        })
    })

    it("should have an object w/ a property of rates w/ the length of 5", ()=>{
        chai.request(serverUrl).get("/rates").end((err,res)=>{
            expect(Object.keys(res.body.rates)).lengthOf(5)
        })
    })
})


